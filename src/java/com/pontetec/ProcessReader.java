/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * A runnable that may be executed as a thread to continuously read an input
 * stream until EOF or a shutdown flag is set.  This is mainly designed to read
 * stdout or stderr of a Java sub-process because the API documentation
 * states that deadlocks may occur if these steams are not read in 
 * a "timely manner".
 * 
 * @author Geoff MacGill (gmacgill@pontetec.com)
 *
 */
public class ProcessReader implements Runnable {
	/* GLOBALS AND DEFULATS */
	private static final String PROCESS_CHARSET = "ISO-8859-1";
	
	/**
	 * Input stream.
	 */
	private BufferedReader inputStream;
	
	/**
	 *  Shutdown flag.
	 */
	private volatile boolean shouldShutdown;
	
	/**
	 * Provides access to logger.
	 */
	private LoggerTranslator logger;

	/**
	 * Creates a new instance of a threaded input stream reader.
	 * @param is The input stream to read.
	 */
    public ProcessReader(InputStream is) {
        this.inputStream = new BufferedReader(new InputStreamReader(is, 
        		Charset.forName(PROCESS_CHARSET)));
        this.logger = LoggerTranslator.getInstance();
        this.shouldShutdown = false;
    }
    
    /**
     * Log the line from the process to the specified backend.  Currently
     * only the Task backend is supported.  Future support may include a file
     * backend with support to choose via constructor.
     * @param line The line to log.
     */
    private void logLine(String line) {
    	this.logger.trace(line);
    }
    
    /**
     * Shutdown the thread safely.
     */
    public void shutdown() {
    	this.shouldShutdown = true;
    }

    /**
     * Starts the thread.
     */
    public void run() {
        try {
            String line = null;
            /* readline() will return null on EOF */
            while ((line = this.inputStream.readLine()) != null 
            		&& !this.shouldShutdown)
            	this.logLine(line);
        }
        catch (IOException ioe) {
            throw new ProcessException("Error reading process stdout/stderr.", ioe);
        }
    }
}
