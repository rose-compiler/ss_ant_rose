/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

/**
 * A runnable that will execute the ROSE compiler framework as a sub-process.
 * The arguments and the source file are provided via the constructor.  This 
 * may optionally be run as a separate thread in a pool of workers to 
 * parallelize the execution of ROSE against a larger set of source files.
 * 
 * @author Geoff MacGill (gmacgill@pontetec.com)
 *
 */
public class RoseCompilerProcess implements Runnable {
	/**
	 * System line separator.
	 */
	private static final String LINE_SEPERATOR = System.getProperty("line.separator");
	
	/**
	 * Base command arguments
	 */
	private final List<String> baseCommand;
	
	/**
	 * The queue from which to get source files
	 */
	private final ArrayList<String> sourceFiles = new ArrayList<String>();
	
	/**
	 * Provides access to logger.
	 */
	private LoggerTranslator logger;

  public String getSourceFilesAsString() {
    String result = "";

    for (String file : this.sourceFiles) {
        result += file + " ";
    }

    return result;
  }

  RoseCompilerProcess(List<String> baseCommand, List<String> sourceFiles) {
      super();
      this.sourceFiles.addAll(sourceFiles);
      this.baseCommand = new ArrayList<String>();
      this.baseCommand.addAll(baseCommand);
      this.baseCommand.addAll(sourceFiles);
      this.logger = LoggerTranslator.getInstance();
  }

	/**
	 * Creates a new instance of the ROSE compiler/translator invoker for the
	 * given Ant task.
	 * 
	 * @param task The Ant task executed/invoked
	 * @param baseCommand The base set of ROSE arguments
	 * @param sourceFile The target source file to compile
	 */
    RoseCompilerProcess(List<String> baseCommand, final String sourceFile) {
        this(baseCommand, new ArrayList<String>() {{ add(sourceFile); }});
    }

    /**
     * Run the ROSE compiler/translator against the target source file.  
     * A RuntimeExectpion of type ProcessException will be thrown on error.
     */
    public void run() {
    	/* log the command to VERBOSE stream */
		this.logRoseCommand(this.baseCommand);
		
		/* invoke ROSE - exceptions handled by callee */
    	this.invokeRose(this.baseCommand);
    }

   /**
    * Invoke the ROSE compiler/translator as a sub-process with the given 
    * arguments.  The first argument should be the location of the 
    * ROSE compiler/translator executable.
    * 
    * @param command The command to execute.
    * @throws ProcessException If failed to execute on the target source file.
    */
	private void invokeRose(List<String> command) throws ProcessException {
		/* create the process output reader thread */
		ProcessReader procOutput = null;
		Thread procOutputThread = null;
		
		try {
			/* create a process builder */
			ProcessBuilder procBuilder = new ProcessBuilder(command);
			
			/* redirect stderr to sdtout to mae reading the output
			 * easier.
			 */
			procBuilder.redirectErrorStream(true);
			
			/* start the process */
			Process proc = procBuilder.start();
			
			/* create and start the stdout/stderr reader.  This will 
			 * only appear of the ant verbose switch is 
			 * provided: "-v" option. */
			procOutput = new ProcessReader(proc.getInputStream());
			procOutputThread = new Thread(procOutput, Thread.currentThread().getName() + "-Reader");
			procOutputThread.start();
			
			/* wait for exit */
			try {
				proc.waitFor();
			} catch (InterruptedException ie) {
				/* set shutdown event on reader */
				procOutput.shutdown();
				/* this will rarely happen unless interrupted by the user. */
				throw new ProcessException("Failed to wait for ROSE completion.  " +
						"Target source file: " + this.getSourceFilesAsString(),
						ie);
			}
			
			try {
				/* wait for the thread to exit */
				procOutputThread.join();
			} catch (InterruptedException ie) {
				/* swallow, we only wanted to attempt a shutdown */
			} finally {
				procOutputThread = null;
				procOutput = null;
			}
			
			/* grab the return code */
			int procReturnCode = proc.exitValue();
			
			/* check the return code */
			if (procReturnCode != 0) {
				/* ROSE returned a non-zero exit code */
				throw new ProcessException("ROSE exited abnormally.  " +
						"Exit Code: " + Integer.toString(procReturnCode) + 
						".  Run build with -v to see stdout/stderr.  " +
						"Target source file: " + this.getSourceFilesAsString());
			}
			
			/* all is OK */
		} catch (IOException ioe) { 
			throw new ProcessException(ioe.getMessage() +
					"  Target source file: " + this.getSourceFilesAsString(), ioe);
		} catch (ProcessException ex) {
			throw ex;
		} catch (RuntimeException ex) {
			throw new ProcessException("Unknown error running ROSE.  " +
					"Target source file: " + this.getSourceFilesAsString(), ex);
		} catch (Exception ex) {
			throw new ProcessException("Unknown error running ROSE.  " +
					"Target source file: " + this.getSourceFilesAsString(), ex);
		} finally {
			/* ensure cleaned up in case of unhandled error */
			if (procOutput != null) {
				procOutput.shutdown();
				try {
					procOutputThread.join();
				} catch (InterruptedException e) {
					/* swallow, we only wanted to attempt a shutdown */
				}
			}
		}
	}
	
	/**
	 * This will print the entire ROSE command if the debug command flag 
	 * is set.
	 * 
	 * Arguments:
	 *     -Drose.compiler.debug.command=TRUE
	 *         Print the ROSE command prior to execution.  This is only
	 *         visible when ant verbose option is set: "-v" option.
	 * @param command The process arguments to be executed
	 */
	private void logRoseCommand(List<String> command) {
		/* use a StringBuilder to more efficient assemble the arguments */
		StringBuilder buffer = new StringBuilder("ROSE compilation arguments:");
		buffer.append(LINE_SEPERATOR);
		
		if (command != null && command.size() > 0) {
			for (String commandArg : command) {
				/* append the arguments */
				buffer.append("    '");
				buffer.append(commandArg);
				buffer.append("'");
				buffer.append(LINE_SEPERATOR);
			}
			
			/* log the full command */
			this.logger.trace(buffer.toString());
		}
	}
}
