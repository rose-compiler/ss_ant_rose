/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

public class ProcessException extends RuntimeException {

	/**
	 * Generated version ID.
	 */
	private static final long serialVersionUID = -8516519498411589419L;

	/**
	 * Runtime exception throw from a thread controlling or interacting with 
	 * a spawned sub-process.
	 */
	public ProcessException() {
		super();
	}
	
	/**
	 * Runtime exception throw from a thread controlling or interacting with 
	 * a spawned sub-process.
	 * @param message Error message
	 */
	public ProcessException(String message) {
		super(message);
	}
	
	/**
	 * Runtime exception throw from a thread controlling or interacting with 
	 * a spawned sub-process. 
	 * @param cause Error cause
	 */
	public ProcessException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Runtime exception throw from a thread controlling or interacting with 
	 * a spawned sub-process.
	 * @param message Error message
	 * @param cause Error cause
	 */
	public ProcessException(String message, Throwable cause) {
		super(message, cause);
	}

}
