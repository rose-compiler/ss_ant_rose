/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CheckedThreadPoolExecutor extends ThreadPoolExecutor {

	private LoggerTranslator logger;
	private boolean jobFailed = false;
	
	public CheckedThreadPoolExecutor(int corePoolSize, 
			int maximumPoolSize, 
			long keepAlivekeepAliveTime,
			TimeUnit unit, 
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, 
				maximumPoolSize, 
				keepAlivekeepAliveTime, 
				unit, 
				workQueue);
		this.logger = LoggerTranslator.getInstance();
	}

	public CheckedThreadPoolExecutor(int corePoolSize, 
			int maximumPoolSize, 
			long keepAlivekeepAliveTime,
			TimeUnit unit, 
			BlockingQueue<Runnable> workQueue, 
			ThreadFactory threadFactory) {
		super(corePoolSize, 
				maximumPoolSize, 
				keepAlivekeepAliveTime, 
				unit, 
				workQueue,
				threadFactory);
		this.logger = LoggerTranslator.getInstance();
	}
	
	public CheckedThreadPoolExecutor(int corePoolSize, 
			int maximumPoolSize, 
			long keepAlivekeepAliveTime,
			TimeUnit unit, 
			BlockingQueue<Runnable> workQueue,
			RejectedExecutionHandler handler) {
		super(corePoolSize, 
				maximumPoolSize, 
				keepAlivekeepAliveTime, 
				unit, 
				workQueue,
				handler);
		this.logger = LoggerTranslator.getInstance();
	}

	public CheckedThreadPoolExecutor(int corePoolSize, 
			int maximumPoolSize, 
			long keepAlivekeepAliveTime,
			TimeUnit unit, 
			BlockingQueue<Runnable> workQueue, 
			ThreadFactory threadFactory,
			RejectedExecutionHandler handler) {
		super(corePoolSize, 
				maximumPoolSize, 
				keepAlivekeepAliveTime, 
				unit, 
				workQueue,
				threadFactory,
				handler);
		this.logger = LoggerTranslator.getInstance();
	}
	
	public boolean hasFailedJobs() {
		return this.jobFailed;
	}
	
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		if (t != null) {
			this.jobFailed = true;
			this.logger.error("Job Failed: " + r.toString(), t);
			List<Runnable> canceledJobs = this.shutdownNow();
			if (canceledJobs != null) {
				for (Runnable canceledJob : canceledJobs) {
					this.logger.error("Job Canceled: " + canceledJob.toString());
				}
			}
		}
	}
}
