/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class JavacOptions {
	
	private Options parserOptions;
	private String source;
	private String target;
	private String encoding;
	private String destDir;
	private String[] classpath;
	private String[] sourcepath;
	private String[] sourceFiles;
	
	public JavacOptions() {
		this.setDefaults();
		this.parserOptions = initOptions();
	}
	
	private void setDefaults() {
		this.source = null;
		this.target = null;
		this.encoding = null;
	}
	
	private Options initOptions() {
		Options opts = new Options();
		
		opts.addOption(makePropertyOption());
		opts.addOption(makeDestDirOption());
		opts.addOption(makeClasspathOption());
		opts.addOption(makeSourcepathOption());
		opts.addOption(makeSourceOption());
		opts.addOption(makeTargetOption());
		opts.addOption(makeEncodingOption());
		opts.addOption(makeHelpOption());
		
		return opts;
	}

	private Option makePropertyOption() {
		/* -Dproperty=value */
		Option property = new Option("D", "use value for given property");
		property.setArgName("property=value");
		property.setValueSeparator('=');
		property.setArgs(2);
		return property;
	}
	
	private Option makeClasspathOption() {
		Option classpath = new Option("cp", "[classpath (default = null)]");
		classpath.setLongOpt("classpath");
		classpath.setRequired(false);
		classpath.setArgs(1);
		return classpath;
	}
	
	private Option makeDestDirOption() {
		Option destDir = new Option("d", "[directory (default = source)]");
		destDir.setRequired(false);
		destDir.setArgs(1);
		return destDir;
	}
	
	private Option makeSourcepathOption() {
		Option sourcepath = new Option("sourcepath", "sourcepath");
		sourcepath.setRequired(true);
		sourcepath.setArgs(1);
		return sourcepath;
	}
	
	private Option makeSourceOption() {
		Option source = new Option("source", "source jvm version");
		source.setRequired(false);
		source.setArgs(1);
		return source;
	}
	
	private Option makeTargetOption() {
		Option target = new Option("target", "target jvm version");
		target.setRequired(false);
		target.setArgs(1);
		return target;
	}
	
	private Option makeEncodingOption() {
		Option encoding = new Option("encoding", "source encoding");
		encoding.setRequired(false);
		encoding.setArgs(1);
		return encoding;
	}
		
	private Option makeHelpOption() {
		Option help = new Option("h", "help");
		help.setLongOpt("help");
		help.setRequired(false);
		help.setArgs(0);
		return help;
	}
	
	public static void outputHelpAndExit(boolean isError) {
		if (isError) {
			System.out.println(RoseCompilerAdapter.ROSE_JAVA_HELP);
			System.exit(1);
		} else {
			System.out.println(RoseCompilerAdapter.ROSE_JAVA_HELP);
			System.exit(0);
		}
	}

	public static void logHelpAndExit(boolean isError) {
		LoggerTranslator logger = LoggerTranslator.getInstance();
		if (isError) {
			logger.error(RoseCompilerAdapter.ROSE_JAVA_HELP);
			System.exit(1);
		} else {
			logger.info(RoseCompilerAdapter.ROSE_JAVA_HELP);
			System.exit(0);
		}
	}
	

	public void parseOptions(String[] arguments) throws IllegalArgumentException, ParseException {
		if (arguments == null)
			throw new IllegalArgumentException("No arguments provided.");
		
		PosixParser parser = new PosixParser();
		CommandLine cli = parser.parse(this.parserOptions, arguments);
	
		if (cli.hasOption("help")) {
			outputHelpAndExit(false);
		}
		
		if (cli.hasOption("sourcepath")) {
			this.sourcepath = this.splitPathValue(cli.getOptionValue("sourcepath"));
		} else {
			throw new IllegalArgumentException("Missing required argument: -sourcepath\n");
		}

		if (cli.hasOption("classpath")) {
			this.classpath = this.splitPathValue(cli.getOptionValue("classpath"));
		}
		
		if (cli.hasOption("encoding")) {
			this.encoding = cli.getOptionValue("encoding");
		} 
		
		if (cli.hasOption("source")) {
			this.source = cli.getOptionValue("source");
		}
		
		if (cli.hasOption("target")) {
			this.target = cli.getOptionValue("target");
		}
		
		if (cli.hasOption("d")) {
			this.destDir = cli.getOptionValue("d");
		}
		
		this.handleProperties(cli);
		
		this.handlePositional(cli.getArgs());

		if (this.sourceFiles == null || this.sourceFiles.length == 0) {
			throw new IllegalArgumentException("Missing required positional argument: <target java files>\n");
		}
	}
	private String[] splitPathValue(String value) {
		return value.split(":");
	}
	
	private void handleProperties(CommandLine cli) {
		if (cli.hasOption("D")) {
			String[] values = cli.getOptionValues("D");
			if (values.length != 0) {
				for (int ii=0; ii < values.length; ii+=2) {
					System.setProperty(values[ii], values[ii+1]);
				}
			}
			return;
		}
	}
	
	private void handlePositional(String[] positionalArguments) {
		if (positionalArguments == null)
			return;
		
		List<String> sourceFiles = new ArrayList<String>();
		
		for (String curArg : positionalArguments) {
			if (curArg.toLowerCase().endsWith(".java")) {
				sourceFiles.add(curArg);
			}
		}
		
		this.sourceFiles = sourceFiles.toArray(new String[0]);
	}
	
	public String getEncoding() {
		return this.encoding;
	}
	
	public String getDestDir() {
		return this.destDir;
	}
	
	public String[] getClasspath() {
		return this.classpath;
	}
	
	public String[] getSourcepath() {
		return this.sourcepath;
	}
	
	public String[] getSourceFiles() {
		return this.sourceFiles;
	}
	
	public String getSource() {
		return this.source;
	}
	
	public String getTarget() {
		return this.target;
	}
}
