/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.compilers.DefaultCompilerAdapter;
import org.apache.tools.ant.types.Path;

public class RoseCompilerAdapter extends DefaultCompilerAdapter {
	
	private static final int ROSE_TRANSLATOR_JOBS_DEFAULT = 1;
	
	/**
	 * The prefix for all ROSE translator arguments.
	 */
	private static final String ROSE_TRANSLATOR_KEY_BASE = "com.pontetec.rosecompiler.";
	private static final String ROSE_TRANSLATOR_ARG_KEY_BASE = ROSE_TRANSLATOR_KEY_BASE + "translator.arg.";
	private static final String ROSE_TRANSLATOR_KEY = ROSE_TRANSLATOR_KEY_BASE + "translator";
	private static final String ROSE_TRANSLATOR_JOBS_KEY = ROSE_TRANSLATOR_KEY_BASE + "translator.jobs";
	private static final String ROSE_TRANSLATOR_USE_SOURCEPATH_KEY = ROSE_TRANSLATOR_KEY_BASE + "translator.usesourcepath";
	private static final String ROSE_TRANSLATOR_APPEND_DEST_PATH_KEY = ROSE_TRANSLATOR_KEY_BASE + "translator.appenddestpath";
	private static final String ROSE_TRANSLATOR_USE_SINGLE_COMMANDLINE_KEY = ROSE_TRANSLATOR_KEY_BASE + "use_single_commandline";
	
	/**
	 * The length of ROSE translator argument prefix.  This will be ignored 
	 * when parsing the ordinal.
	 */
	private static final int ROSE_TRANSLATOR_ARG_KEY_BASE_LENGTH = ROSE_TRANSLATOR_ARG_KEY_BASE.length();
	
	/**
	 * System line separator.
	 */
	private static final String LINE_SEPERATOR = System.getProperty("line.separator");
	
	/* Properties */
	private int roseTranslatorJobs = 1;
	private String roseTranslator = null;
	private List<String> roseTranslatorArguments;
	private boolean useSourcepath = true;
	private boolean appendDestToClasspath = false;
	private boolean useSingleCommandline = false;
	private LoggerTranslator logger = null;
	private JavacOptions cliOptions = null;
	
	private final boolean runAsAnt;
	
	public static final String ROSE_JAVA_HELP = 
			"Usage:\n"
			+ "COMMAND ARGUMENTS COMMAND_SPECIFIC_ARGUMENTS\n"
			+ "\n"
			+ "COMMAND = java (for standalone) or ant (for part of build process)\n"
			+ "\n"
			+ "ARGUMENTS\n"
			+ "    -D" + ROSE_TRANSLATOR_KEY + "=<PATH TO TRANSLATOR EXECUTABLE>\n"
			+ "    [-D" + ROSE_TRANSLATOR_ARG_KEY_BASE + "<FLAG_1>=<VALUE>\n"
			+ "    [-D" + ROSE_TRANSLATOR_ARG_KEY_BASE + "<FLAG_2>=<VALUE>\n"
			+ "    [-D" + ROSE_TRANSLATOR_ARG_KEY_BASE + "<FLAG_N>=<VALUE>\n"
			+ "    [-D" + ROSE_TRANSLATOR_JOBS_KEY + "=<CONCURRENT ROSE JOBS> (default 1)]\n"
			+ "    [-D" + ROSE_TRANSLATOR_USE_SOURCEPATH_KEY + "=<true/yes>] (default false/no)]\n"
			+ "    [-D" + ROSE_TRANSLATOR_APPEND_DEST_PATH_KEY + "=<true/yes>] (default false/no)]\n"
			+ "    [-D" + ROSE_TRANSLATOR_USE_SINGLE_COMMANDLINE_KEY + "=<true/yes>] (default false/no)]\n"
			+ "\n"
			+ "    ROSE Specific Flags/Arguments:\n"
			+ "        Passing any arguments directly to ROSE is supported via the \n"
			+ "        \"-D" + ROSE_TRANSLATOR_ARG_KEY_BASE + "<FLAG_N>=<VALUE>\" set\n"
			+ "        of arguments.  These will be parsed and processed as follows.\n"
			+ "            1. FLAG_N should be a string matching the ROSE specific flag\n"
			+ "               (e.g.) -rose:java would take the form of \n"
			+ "               \"-D" + ROSE_TRANSLATOR_ARG_KEY_BASE + "rose.java\".  A\n"
			+ "               dash (-) will be prepended and periods (.) will be replaced\n"
			+ "               by a colon (:).\n"
			+ "            2. VALUE will be processed as a literal except in the case that\n"
			+ "               it is equal to TRUE or FALSE.  These values allow for appending\n"
			+ "               a flag without a parameter, or to explicitly removed if previously\n"
			+ "               defined.  Note, that TRUE is equivalent to no VALUE specificed.\n"
			+ "\n"
			+ "<ant command-specific arguments>\n"
			+ "    -Dbuild.compiler=com.pontetec.RoseCompilerAdapter\n"
			+ "    -lib <library containing jar files>\n"
			+ "\n"
			+ "<java command-specific arguments>\n"
			+ "    com.pontetec.Main\n"
			+ "    [-source <source version>]     (default = java.version)\n"
			+ "    [-target <target version>]     (default = java.version)\n"
			+ "    [-encoding <source encoding>]  (default = UTF-8)\n"
			+ "    [-classpath <classpath>]       (default = null)\n"
			+ "    [-log <logfile>]               (default = stderr)\n"
			+ "    -sourcepath <sourcepath>       directory containing target package\n" 
			+ "    <target java files>\n"
			+ "\n";
// TODO Add examples
//			+ "Example:\n"
//			+ "ant -v \n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_MODE_KEY + "=inject\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_FILENAME_KEY + "=Main.java\n"		
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_TYPE_KEY + "=Main\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_METHODNAME_KEY + "=main\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_METHODSIGNATURE_KEY + "=String[]\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_WEAKNESS_SNIPPET_KEY + "=\"/Users/swood/Documents/workspace/ss_java_cwe/src/Tainted_Data/CWE_015/CWE_015_0.java\"\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_DEST_KEY + "=\"/Users/swood/Documents/workspace/RoseTest2\"\n"
//			+ "    -lib ../ss_vinject4j/dist/lib\n"
//			+ "    -Dbuild.compiler=com.pontetec.VinjectCompilerAdapter\n"
//			+ "\n" 
//			+ "java\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_MODE_KEY + "=inject\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_FILENAME_KEY + "=Main.java\n"		
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_TYPE_KEY + "=Main\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_METHODNAME_KEY + "=main\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_METHODSIGNATURE_KEY + "=String[]\n"
//			+ "    -D" + VINJECT4J_TRANSLATOR_INJECT_WEAKNESS_SNIPPET_KEY + "=\"/Users/swood/Documents/workspace/ss_java_cwe/src/Tainted_Data/CWE_015/CWE_015_0.java\"\n"
//			+ "    com.pontetec.Main\n"
//			+ "    -sourcepath ../RoseTest/src/\n"
//			+ "    ../RoseTest/src/com/pontetec/Main.java\n";
	
	/**
	 * The default constructor.  This should only ever be called by Ant
	 * when running in the context of an Ant build script (build.xml).
	 */
	public RoseCompilerAdapter() {
		this.runAsAnt = true;
	}
	
	/**
	 * The constructor used when running this from the command line directly.
	 * NOTE: This is highly experimental.
	 * @param options
	 */
	public RoseCompilerAdapter(JavacOptions options) {
		this.runAsAnt = false;
		this.cliOptions = options;
	}
	
	/**
	 * Gets the path to the ROSE translator.
	 * @return
	 */
	public String getTranslator() {
		return this.roseTranslator;
	}
	
	/**
	 * Parses the ROSE compiler adapter properties from either Ant or the System.
	 * The behavior depends on the runtime.
	 */
	private void readPropertyValues() {
		this.roseTranslator = this.readRoseTranslatorProperty();
		this.roseTranslatorArguments = this.readRoseArgumentProperties();
		this.roseTranslatorJobs = this.readRoseTranslatorJobsProperty();
		this.appendDestToClasspath = this.readRoseAppendDestPathProperty();
		this.useSingleCommandline = this.readUseSingleCommandlineProperty();
		//this.useSourcepath = this.readRoseUseSourcepathProperty();
	}
	
	/**
	 * Gets the target property by key.
	 * @param key Property name
	 * @return Property value
	 */
	private String getProperty(String key) {
		if (runAsAnt) {
			return this.project.getProperty(key);
		} else {
			return System.getProperty(key);
		}
	}	
	
	/**
	 * Gets the target property by key.  Throws if not set.
	 * @param key Property name
	 * @param required Property is required (TRUE/FALSE)
	 * @return Property value
	 * @throws IllegalArgumentException Required property is not set
	 */
	private String getProperty(String key, boolean required) 
			throws IllegalArgumentException {
		String value = getProperty(key);
		if (required && value == null) {
			throw new IllegalArgumentException("Missing required argument: -D" + key + "\n");
		}
		return value;
	}
	
	private String readRoseTranslatorProperty() throws IllegalArgumentException {
		String argTranslator = this.getProperty(ROSE_TRANSLATOR_KEY, true);
		File targetTranslator = new File(argTranslator);
		if (!targetTranslator.canExecute()) {
			throw new IllegalArgumentException(
					"Required argument -D" + ROSE_TRANSLATOR_KEY + 
					" must reference an executable.");
		}
		return targetTranslator.getAbsolutePath();
	}
	
	private int readRoseTranslatorJobsProperty() throws IllegalArgumentException {
		/* get the number of jobs */
		int translatorJobs = ROSE_TRANSLATOR_JOBS_DEFAULT;
		String translatorJobsStr = this.getProperty(ROSE_TRANSLATOR_JOBS_KEY);
		
		if (translatorJobsStr != null) {
			String tempTrimmed = translatorJobsStr.trim();
			/* try to parse the string to an integer value for sorting */
			translatorJobs = 1;
			try {
				translatorJobs = Integer.parseInt(tempTrimmed);
			} catch (NumberFormatException nfe) {
				throw new IllegalArgumentException(
						"Optional argument -D" + ROSE_TRANSLATOR_JOBS_KEY + 
						" must be set to a positive integer value.");
			}
			
			if (translatorJobs < 1) {
				throw new IllegalArgumentException(
						"Optional argument -D" + ROSE_TRANSLATOR_JOBS_KEY + 
						" must be set to a positive integer value.");
			}
		}
		
		return translatorJobs;
	}
	
	private boolean readRoseUseSourcepathProperty() throws IllegalArgumentException {
		boolean useSourcepath = false;
		
		String useSourcepathStr = this.getProperty(ROSE_TRANSLATOR_USE_SOURCEPATH_KEY);
		if (useSourcepathStr != null) {
			String tempTrimmed = useSourcepathStr.trim();
			if (tempTrimmed.equalsIgnoreCase("true") || tempTrimmed.equalsIgnoreCase("yes"))
				useSourcepath = true;
			else
				useSourcepath = false;
		}
		
		return useSourcepath;
	}
	
	private boolean readRoseAppendDestPathProperty() throws IllegalArgumentException {
		boolean appendDestPath = false;
		
		String appendDestPathStr = this.getProperty(ROSE_TRANSLATOR_APPEND_DEST_PATH_KEY);
		if (appendDestPathStr != null) {
			String tempTrimmed = appendDestPathStr.trim();
			if (tempTrimmed.equalsIgnoreCase("true") || tempTrimmed.equalsIgnoreCase("yes"))
				appendDestPath = true;
			else
				appendDestPath = false;
		}
		
		return appendDestPath;
	}

	private boolean readUseSingleCommandlineProperty() throws IllegalArgumentException {
		boolean useSingleCommandline = false;
		
		String useSingleCommandlineStr =
        this.getProperty(ROSE_TRANSLATOR_USE_SINGLE_COMMANDLINE_KEY);

		if (useSingleCommandlineStr != null) {
			String tempTrimmed = useSingleCommandlineStr.trim();
			if (tempTrimmed.equalsIgnoreCase("true") || tempTrimmed.equalsIgnoreCase("yes"))
				useSingleCommandline = true;
			else
				useSingleCommandline = false;
		}
		
		return useSingleCommandline;
	}

	/**
	 * Called by Ant when the compiler should execute (i.e. build a 
	 * set of Java source files).  This will be essentially forked to a 
	 * sub-process handled by ROSE.
	 * 
	 * Most of the ROSE specific arguments generated from the Ant build 
	 * script (or the CLI options if running via that entry point).
	 * However, additional arguments may be set as follows:  
	 * 
	 * Arguments:
	 *     -Dcom.pontetec.rosecompiler.translator=/opt/rose/bin/myCompiler
	 *         Absolute path to ROSE translator executable.
	 *     
	 *     Extra Arguments
	 *     -Dcom.pontetec.rosecompiler.translator.arg.SWITCH="VALUE"
	 *     -Dcom.pontetec.rosecompiler.translator.arg.SWITCH:2="VALUE"
	 *      Example:
	 *         -Dcom.pontetec.rosecompiler.translator.arg.rose.verbose="TRUE"
	 *     
	 *     Compilation Behavior:
	 *     -Dcom.pontetec.rosecompiler.translator.jobs=n
	 *         Number of connurent jobs to run (think make -j)
	 *     -Dcom.pontetec.rosecompiler.translator.usesourcepath=TRUE
	 *         Use the -rose:java:source switch, this wasn't functional
	 *         in the first release, so we merge to classpath
	 *     -Dcom.pontetec.rosecompiler.translator.appenddestpath=TRUE
	 *         Append -d dest to classpath vs the default of prepend.
	 *     
	 */
	public boolean execute() throws BuildException {
		/* initialize the logger */
		if (this.runAsAnt) {
			LoggerTranslator.initialize(this.getJavac());
		}
		this.logger = LoggerTranslator.getInstance();
		
		/* log which compiler we are using */
		this.logger.trace("Using ROSE compiler");
		
		/* parse the ROSE arguments provided to ant as properties.
		 * these will include the translator executable and the 
		 * ordered set of arguments.  The "-rose:java" argument will
		 * be explicity added, and does not need to be provided.
		 */
		this.readPropertyValues();
		List<String> baseCommand = new ArrayList<String>();
		baseCommand.add(this.roseTranslator);
		baseCommand.addAll(this.roseTranslatorArguments);
		
		/* get the ROSE javac specific command line arguments.  Originally,
		 * this was provided by the base implementation of the Javac taskdef 
		 * in Apache Ant.  However, ROSE requires that most javac specific 
		 * arguments be prefixed with "-rose:java:<javac_switch>".  Thus,
		 * "-classpath" becomes "-rose:java:classpath".
		 */
		List<String> args = generateRoseJavaCommandLine();
		
		/* prep the arguments, merging the javac args with the rose args */
		baseCommand.addAll(args);
		
		/* log source files to compile */
		List<File> resolvedCompileList = null;
		if (this.runAsAnt) {
			resolvedCompileList = Arrays.asList(this.compileList);
			this.logFilesToCompile(resolvedCompileList);
		} else {
			resolvedCompileList = new ArrayList<File>();
			for (String sourceFile : this.cliOptions.getSourceFiles()) {
				resolvedCompileList.add(new File(sourceFile));
			}
		}
		
		/* overall result, exceptions will set this to false */
		boolean result = true;
		
		/* get the effective number of jobs, since we don't want 
		 * to spawn more threads then necessary.
		 */
		int roseTranslatorJobsEffective = Math.min(this.roseTranslatorJobs, resolvedCompileList.size());
		
		/* determine if we are executing inline or multiprocessing mode */
		if (this.useSingleCommandline == true) {
			this.logger.trace("Running single commandline job.");
			try {
				this.executeSingleCommandline(baseCommand, resolvedCompileList);
			} catch (BuildException e) {
				result = false;
				throw e;
			}
		} else if (roseTranslatorJobsEffective == 1) {
			/* execute inline, meaning that this thread synchronously
			 * executes sub-process calls to the specified ROSE translator.
			 * only one invocation of ROSE will run at a time (zeo concurrency).
			 */
			this.logger.trace("Running single job.");
			try {
				this.executeInline(baseCommand, resolvedCompileList);
			} catch (BuildException e) {
				result = false;
				throw e;
			}
		} else {
			/* execute with n number of concurrent invocations of ROSE. */
			this.logger.trace("Running " + roseTranslatorJobsEffective + " jobs.");
			try {
				this.executeConcurrent(baseCommand, resolvedCompileList, roseTranslatorJobsEffective);
			} catch (BuildException e) {
				result = false;
				throw e;
			}
		}
		
		return result;
	}

	private void executeSingleCommandline(List<String> baseCommand, List<File> compileList) throws BuildException {
    ArrayList<String> files = new ArrayList<String>();
    for (File sourceFile : compileList) {
          files.add(sourceFile.getAbsolutePath());
    }

    RoseCompilerProcess translator =
        new RoseCompilerProcess(baseCommand, files);
    try {
      translator.run();
    } catch (ProcessException ex) {
      throw new BuildException("Job Failed: " + translator.toString(), ex);
    }
	}
	
	private void executeInline(List<String> baseCommand, List<File> compileList) throws BuildException {
		/* compile each file serially in this thread */
		for (File sourceFile : compileList) {
			RoseCompilerProcess translator = 
					new RoseCompilerProcess(baseCommand, sourceFile.getAbsolutePath());
			try {
				translator.run();	
			} catch (ProcessException ex) {
				throw new BuildException("Job Failed: " + translator.toString(), ex);
			}
		}
	}
	
	private void executeConcurrent(List<String> baseCommand, List<File> compileList, int jobs) throws BuildException {
		/* create an unbounded* queue */
		/* technically bound to Integer.MAX_VALUE */
		LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(); 
		
		/* create the thread pool executor service */
		CheckedThreadPoolExecutor executor = 
				new CheckedThreadPoolExecutor(jobs, jobs, 
						1000L, TimeUnit.MILLISECONDS, workQueue,
						new RejectedExecutionHandler() {
							/**
							 * This will only be executed if one or more jobs failed,
							 * causing a premature shutdown of the queue/executor.
							 */
							@Override
							public void rejectedExecution(Runnable r, ThreadPoolExecutor pool) {
								LoggerTranslator.getInstance().error("Job Rejected: " + r.toString());
							}
						});
		
		/* this is optional.  we could instead allow the 
		 * threadpoolexecutor to create threads as needed.
		 * However, we anticipate needing all threads since 
		 * the target java projects are LARGE.
		 */
		executor.prestartAllCoreThreads();
		
		/* queue all the source files in the compile list with the 
		 * executor.
		 */
		for (File sourceFile : compileList) {
			executor.execute(new RoseCompilerProcess(baseCommand, 
					sourceFile.getAbsolutePath()));
		}
		
		/* shutdown the executor.  current queue will be processed,
		 * but no new jobs will be added.
		 */
		executor.shutdown();
		
		/* wait for termination of all tasks */
		try {
			while (!executor.awaitTermination(1000L, TimeUnit.MILLISECONDS)) {
				/* loop until terminated */
			}
		} catch (InterruptedException ex) {
			throw new BuildException("Failed to wait for tasks to terminate.", ex); 
		}
		
		if (executor.hasFailedJobs()) {
			throw new BuildException("One or more jobs failed.  As a result, remaining jobs may have been canceled or skipped.");
		}
	}
	
	/**
	 * Parses the "-Dcom.pontetec.rosecompiler.translator.arg." prefixed properties
	 * and values.  The resulting list is returned.
	 * @return
	 */
	private List<String> readRoseArgumentProperties() {
		/* list of results */
		List<String> resolvedRoseArguments = new ArrayList<String>();
		
		/* create the arguments list */
		Hashtable<String, Object> parsedArguments = new Hashtable<String, Object>();
		
		/* add the standard java arguments */
		parsedArguments.put("-rose:java", new Boolean(true));  /* set that this is java */
		
		/* get the ant project properties - raw map */
		/* note the "?" because Ant and System.getProperty() differ 
		 * in the implementation of Hashtable<K, V> signature.
		 */
		Hashtable<?, ?> properties = null;
		if (this.runAsAnt) {
			properties = this.project.getProperties();
		} else {
			properties = System.getProperties();
		}
		Enumeration<?> propertiesItr = properties.keys();
		
		/* iterate properties and grab all "rose.compiler.translator.arg." 
		 * prefixed properties.
		 */
		while (propertiesItr.hasMoreElements()) {
			String itemKey = ((String)propertiesItr.nextElement());
			if (itemKey.toLowerCase().startsWith(ROSE_TRANSLATOR_ARG_KEY_BASE)) {
				/* get the argument ordinal and add it to the sorted map */
				String argumentKey = itemKey.substring(ROSE_TRANSLATOR_ARG_KEY_BASE_LENGTH);
				if (argumentKey == null || argumentKey.isEmpty()) {
					/* this is empty, for some reason */
					throw new BuildException("ROSE compiler argument incorrectly specified.  Property key: " + itemKey, this.location);
				}
				
				/* convert the argument key to a rose flag */
				String roseArgumentFlag = "-" + argumentKey.replace('.', ':'); /* don't convert to lower as some rose options may include mixed case */
				Object roseArgumentValue = properties.get(itemKey);
				boolean removeFlag = false;
				
				/* if the value is the boolean TRUE or FALSE, then we treat this as 
				 * a flag without parameters.
				 */
				if (roseArgumentValue != null) {
					/* trim the value for comparison */
					String trimmedValue = ((String)roseArgumentValue).trim();
					
					if (trimmedValue.isEmpty()) {
						/* empty value, so do not set */
						roseArgumentValue = new Boolean(true);
					} else if (trimmedValue.equalsIgnoreCase("true") || trimmedValue.equalsIgnoreCase("yes")) {
						/* flag is enabled */
						roseArgumentValue = new Boolean(true);
					} else if (trimmedValue.equalsIgnoreCase("false") || trimmedValue.equalsIgnoreCase("no")) {
						/* flag is explicity disabled */
						removeFlag = true; 
					}
				}
				
				/* remove or add the flag and value */
				if (removeFlag) {
					if (parsedArguments.containsKey(roseArgumentFlag))
						parsedArguments.remove(roseArgumentFlag);
				} else {
					parsedArguments.put(roseArgumentFlag, roseArgumentValue);
				}
			}
		}
		
		/* add translator args if present */
		Set<Map.Entry<String, Object>> parsedArgumentPairs = parsedArguments.entrySet();
		Iterator<Map.Entry<String, Object>> parsedArgumentPairsItr = parsedArgumentPairs.iterator();
		while (parsedArgumentPairsItr.hasNext()) {
			Map.Entry<String, Object> roseArgumentPair = parsedArgumentPairsItr.next();
			resolvedRoseArguments.add(roseArgumentPair.getKey());
			Object argValue = roseArgumentPair.getValue();
			if (argValue instanceof String)
				resolvedRoseArguments.add((String)argValue);
		}
		
		return resolvedRoseArguments;
	}
	
	/**
	 * Log the files to be compiled by ROSE.  The files will be logged 
	 * to the ant task log only if verbose mode is specififed.
	 */
    private void logFilesToCompile(List<File> compileList) {
    	/* create a buffer, this is more efficient */
        StringBuffer sourceFiles = new StringBuffer("File(s) to be compiled:");
        
        /* the files will be new-line separated */
        sourceFiles.append(LINE_SEPERATOR);

        /* loop over the array of files, grabbing the absolute path for each */
        for (File compileTarget : compileList) {
            String filePath = compileTarget.getAbsolutePath();
            sourceFiles.append("    ");  /* indent */
            sourceFiles.append(filePath);  /* add the file */ 
            sourceFiles.append(LINE_SEPERATOR);  /* add a line sperator */
        }
        
        /* log the list of files, but only in verbose mode */
        this.attributes.log(sourceFiles.toString(), Project.MSG_VERBOSE);
    }

    /**
     * Processes the Ant project properties and creates the ROSE command line
     * switches.  Doesn't add the files to be compiled, as this is done later.
     * @return the command line flags
     */
    protected List<String> generateRoseJavaCommandLineFromAnt() {
    	/* the arguments list */
    	List<String> command = new ArrayList<String>();
    	
    	/* grab the task classpath.  We will optionally override the 
    	 * formation of this to append the classes folder.
    	 */
        String classpath = this.getRoseCompileClasspath();
        
        // For -rose:java:sourcepath, use the "sourcepath" value if present.
        // Otherwise default to the "srcdir" value.
        Path sourcepath = null;
        if (this.compileSourcepath != null) {
            sourcepath = this.compileSourcepath;
        } else {
            sourcepath = this.src;
        }
        
        /* add the destination directory */
        if (this.destDir != null) {
        	command.add("-rose:java:d");
            command.add(this.destDir.getAbsolutePath());
        }

        command.add("-rose:java:classpath");
        command.add(classpath);
        
        /* Even though older versions of javac do no support -sourcepath option,
         * ROSE does and has only been tested against Java 1.6+.  If we decided to 
         * support older java versions we'll need a way to test the config for what 
         * the backend compiler is and then change how we handle some options, 
         * collapsing some options to the classpath.
         */
        
        /* only use sourcepath if option is specified and not empty. */
        if (this.useSourcepath && sourcepath.size() > 0) {
        	command.add("-rose:java:sourcepath");
        	command.add(sourcepath.toString());
        }
        
        /* set target */
        if (this.target != null) {
        	command.add("-rose:java:target");
        	command.add(this.target);
        }
        
        /* set source */
        if (this.attributes.getSource() != null) {
        	command.add("-rose:java:source");
        	command.add(this.adjustSourceValue(attributes.getSource()));
        } else if (this.target != null) {
        	/* this will cause a warning, because source should be set 
        	 * if target is set.
        	 */
            setImplicitSourceSwitch(command, 
            		this.target, 
            		this.adjustSourceValue(this.target));
        }

        return command;
    }
    
    /**
     * Processes the CLI arguments and creates the ROSE command line
     * switches.  Doesn't add the files to be compiled, as this is done later.
     * @return the command line flags
     */
    protected List<String> generateRoseJavaCommandLineFromCli() {
    	/* the arguments list */
    	List<String> command = new ArrayList<String>();
    	
    	/* grab the task classpath.  We will optionally override the 
    	 * formation of this to append the classes folder.
    	 */
        String classpath = this.getRoseCompileClasspath();
        
        // For -rose:java:sourcepath, use the "sourcepath" value if present.
        // Otherwise default to the "srcdir" value.
        String sourcepath = null;
        if (this.cliOptions.getSourcepath() != null &&
        		this.cliOptions.getSourcepath().length != 0) {
            List<String> sourcepaths = Arrays.asList(this.cliOptions.getSourcepath());
            boolean isFirst = true;
            StringBuilder sourcepathStr = new StringBuilder();
            for (String path : sourcepaths) {
            	if (!isFirst) {
            		sourcepathStr.append(":");
            	} else {
            		isFirst = false;
            	}
            	sourcepathStr.append(path);
            }
            sourcepath = sourcepathStr.toString();
        }
        
        /* add the destination directory */
        if (this.cliOptions.getDestDir() != null) {
        	command.add("-rose:java:d");
            command.add(this.cliOptions.getDestDir());
        }

        command.add("-rose:java:classpath");
        command.add(classpath);
        
        /* Even though older versions of javac do no support -sourcepath option,
         * ROSE does and has only been tested against Java 1.6+.  If we decided to 
         * support older java versions we'll need a way to test the config for what 
         * the backend compiler is and then change how we handle some options, 
         * collapsing some options to the classpath.
         */
        
        /* only use sourcepath if option is specified and not empty. */
        if (this.useSourcepath && sourcepath != null) {
        	command.add("-rose:java:sourcepath");
        	command.add(sourcepath);
        }
        
        /* set target */
        if (this.cliOptions.getTarget() != null) {
        	command.add("-rose:java:target");
        	command.add(this.cliOptions.getTarget());
        }
        
        /* set source */
        if (this.cliOptions.getSource() != null) {
        	command.add("-rose:java:source");
        	command.add(this.adjustSourceValue(this.cliOptions.getSource()));
        } else if (this.cliOptions.getTarget() != null) {
        	/* this will cause a warning, because source should be set 
        	 * if target is set.
        	 */
            setImplicitSourceSwitch(command, 
            		this.cliOptions.getTarget(), 
            		this.adjustSourceValue(this.cliOptions.getTarget()));
        }

        return command;
    }

    /**
     * Does the command line argument processing for javac options 
     * mapped to ROSE java options.
     * @return the command line
     */
    protected List<String> generateRoseJavaCommandLine() {
        if (this.runAsAnt) {
        	return this.generateRoseJavaCommandLineFromAnt();
        } else {
        	return this.generateRoseJavaCommandLineFromCli();
        }
    }
    
    /**
     * Build the compile classpath from Ant or the command line options.
     */
    private String getRoseCompileClasspath() {
    	if (this.runAsAnt) {
    		return this.getAntCompileClasspath();
    	} else {
    		return this.getCliCompileClasspath();
    	}
    }
    
    /**
     * Builds the compilation classpath (Ant).
     * @return the compilation class path
     */
    private String getAntCompileClasspath() {
    	Path classpath = new Path(this.project);

    	/* prepend dest to classpath, unless otherwise overridden.
    	 * this allows for .class files still valid to not be recompiled.
    	 * the defualt for ant is to prepend, so we will keep it this way. */
        if (!this.appendDestToClasspath && 
        		this.destDir != null && 
        		this.getJavac().isIncludeDestClasses()) {
        	this.logger.trace("Prepending destination to compile classpath.");
            classpath.setLocation(this.destDir);
        }
        
        /* check if we should add sourcepath to classpath */
        if (!this.useSourcepath) {
        	this.logger.trace("Adding sourcepath to compile classpath.");
        	Path sourcepath = null;
            if (this.compileSourcepath != null) {
                sourcepath = this.compileSourcepath;
            } else {
                sourcepath = this.src;
            }
            
            /* add if not empty */
            if (sourcepath.size() > 0) {
            	classpath.add(sourcepath);
            }
        }

        // Combine the build classpath with the system classpath, in an
        // order determined by the value of build.sysclasspath
        Path cp = this.compileClasspath;
        if (cp == null) {
            cp = new Path(this.project);
        }
        if (this.includeAntRuntime) {
            classpath.addExisting(cp.concatSystemClasspath("last"));
        } else {
            classpath.addExisting(cp.concatSystemClasspath("ignore"));
        }

        if (this.includeJavaRuntime) {
            classpath.addJavaRuntime();
        }
        
        /* check if we should append dest to classpath vs. prepend (above) */
        if (this.appendDestToClasspath && 
        		this.destDir != null && 
        		this.getJavac().isIncludeDestClasses()) {
        	this.logger.trace("Appending destination to compile classpath.");
            classpath.setLocation(this.destDir);
        }

        return classpath.toString();
    }
    
    /**
     * Builds the compilation classpath (CLI).
     * @return the compilation class path
     */
    private String getCliCompileClasspath() {
    	List<String> classpath = new ArrayList<String>();
    	final String delim = ":";

    	/* prepend dest to classpath, unless otherwise overridden.
    	 * this allows for .class files still valid to not be recompiled.
    	 * the defualt for ant is to prepend, so we will keep it this way. */
        if (!this.appendDestToClasspath && 
        		this.cliOptions.getDestDir() != null) {
        	this.logger.trace("Prepending destination to compile classpath.");
            classpath.add(this.cliOptions.getDestDir());
        }
        
        /* check if we should add sourcepath to classpath */
        if (!this.useSourcepath) {
            if (this.cliOptions.getSourcepath() != null &&
            		this.cliOptions.getSourcepath().length != 0) {
            	this.logger.trace("Adding sourcepath to compile classpath.");
            	classpath.addAll(Arrays.asList(this.cliOptions.getSourcepath()));
            }
        }
        
        /* check if we should append dest to classpath vs. prepend (above) */
        if (this.appendDestToClasspath && 
        		this.destDir != null && 
        		this.getJavac().isIncludeDestClasses()) {
        	this.logger.trace("Appending destination to compile classpath.");
        	classpath.add(this.cliOptions.getDestDir());
        }
        
        boolean isFirst = true;
        StringBuilder classpathStr = new StringBuilder();
        for (String path : classpath) {
        	if (!isFirst) {
        		classpathStr.append(delim);
        	} else {
        		isFirst = false;
        	}
        	classpathStr.append(path);
        }
        return classpathStr.toString();
    }
    
    private void setImplicitSourceSwitch(List<String> command,
    		String target, String source) {
		this.logger.warn("");
		this.logger.warn("          WARNING");
		this.logger.warn("");
		this.logger.warn("The -rose:java:source switch defaults to target " 
				+ target + ".");
		this.logger.warn("If you specify -rose:java:target " + target
				+ " you now must also specify -rose:java:source " + source
				+ ".");
		this.logger.warn("Ant will implicitly add -rose:java:source " + source
				+ " for you.  Please change your build file.");
		command.add("-rose:java:source");
		command.add(source);
	}
    
    private String adjustSourceValue(String source) {
        return (source.equals("1.1") || source.equals("1.2")) ? "1.3" : source;
    }
}
