/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

public class LoggerTranslator {
	
	/* Singleton constructs */
	private static final ReentrantLock singletonLock = new ReentrantLock();
	private static LoggerTranslator instance = null;
	
	static void initailize() {
		singletonLock.lock();
		try {
			if (instance == null) {
				instance = new LoggerTranslator();
			}
		} finally {
			singletonLock.unlock();
		}
	}
	
	static void initialize(Task task) {
		singletonLock.lock();
		try {
			if (instance != null) {
				if (!instance.task.equals(task)) {
					instance = new LoggerTranslator(task);
				}
			} else {
				instance = new LoggerTranslator(task);
			}
		} finally {
			singletonLock.unlock();
		}
	}
	
	/**
	 * The singleton instance of the logger.
	 * @return
	 */
	public static LoggerTranslator getInstance() {
		return instance;
	}
	
	private boolean isAntTask;
	private Task task;
	private Logger logger;
	
	/**
	 * Constructs a logger that will by default write messages to STDOUT/CONSOLE.
	 */
	private LoggerTranslator() {
		this.isAntTask = false;
		this.task = null;
		this.logger = LogManager.getLogger("ROSE");
	}
	
	/**
	 * Constructs a logger that will proxy all messages to the Ant task.
	 * @param task The context Ant task running.
	 */
	private LoggerTranslator(Task task) {
		this.isAntTask = true;
		this.task = task;
		this.logger = null;
	}
	
	/**
	 * Logs the target message to Ant, translating the logging level.
	 * @param level Log level
	 * @param message The message to log
	 * @param error The cause, if any
	 */
	private void logToAnt(Level level, String message, Throwable error) {
		int antLevel = Project.MSG_WARN;
		switch (level) {
			case DEBUG:
				antLevel = Project.MSG_DEBUG;
				break;
			case WARN:
				antLevel = Project.MSG_WARN;
				break;
			case FATAL:
			case ERROR:
				antLevel = Project.MSG_ERR;
				break;
			case INFO:
				antLevel = Project.MSG_INFO;
				break;
			case TRACE:
				antLevel = Project.MSG_VERBOSE;
				break;
		}
		if (error == null) {
			this.task.log(message, antLevel);
		} else {
			this.task.log(message, error, antLevel);
		}
	}
	
	public void log(Level level, String message, Throwable error) {
		if (this.isAntTask) {
			this.logToAnt(level, message, error);
		} else {
			this.logger.log(level, message, error);
		}
	}
	
	public void log(Level level, String message) {
		if (this.isAntTask) {
			this.logToAnt(level, message, null);
		} else {
			this.logger.log(level, message);
		}
	}
	
	public void debug(String message, Throwable error) {
		this.log(Level.DEBUG, message, error);
	}
	
	public void debug(String message) {
		this.log(Level.DEBUG, message);
	}
	
	public void error(String message, Throwable error) {
		this.log(Level.ERROR, message, error);
	}
	
	public void error(String message) {
		this.log(Level.ERROR, message);
	}
	
	public void warn(String message, Throwable error) {
		this.log(Level.WARN, message, error);
	}
	
	public void warn(String message) {
		this.log(Level.WARN, message);
	}
	
	public void info(String message, Throwable error) {
		this.log(Level.INFO, message, error);
	}
	
	public void info(String message) {
		this.log(Level.INFO, message);
	}
	
	public void trace(String message, Throwable error) {
		this.log(Level.TRACE, message, error);
	}
	
	public void trace(String message) {
		this.log(Level.TRACE, message);
	}
}
