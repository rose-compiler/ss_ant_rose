/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec;

import org.apache.commons.cli.ParseException;

public class Main {
	
	/**
	 * CLI entry point for the Rose Compiler Adapter.  Attempts to mimic
	 * basic javac options to support translation with a ROSE compiler
	 * backend.  NOTE: Not all javac options are supported at this time.
	 * @param args
	 */
	private Main(String[] args) {
		JavacOptions arguments = null;
		RoseCompilerAdapter adapter = null;
		LoggerTranslator logger = LoggerTranslator.getInstance();
		boolean parsedOptions = false;
		
		arguments = new JavacOptions();
		
		try {
			arguments.parseOptions(args);
			parsedOptions = true;
		} catch (IllegalArgumentException ex) {
			logger.error("Invalid options.", ex);
		} catch (ParseException ex) {
			logger.error("Failed to parse options.", ex);
		}
		
		if (!parsedOptions) {
			System.exit(1);
		}
		
		try {
			adapter = new RoseCompilerAdapter(arguments);
			
			adapter.execute();
		} catch (Exception ex) {
			logger.error("Unexpected error occurred.", ex);
			System.exit(1);
		}
		
		System.exit(0);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LoggerTranslator.initailize();
		if (args == null || args.length == 0)
			JavacOptions.outputHelpAndExit(false);
		new Main(args);
		System.exit(0);
	}

}
