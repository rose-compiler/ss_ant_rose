/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec.test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.cli.ParseException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.pontetec.JavacOptions;

public class JavacOptionsTest {
	@Rule
    public TemporaryFolder folder = new TemporaryFolder();
	
	@Test
	public void testRequiredOptions() {
		String[] args = new String[] {
				"-sourcepath", 
				new File(folder.getRoot(), "src").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/Main.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/FileTest.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/TestOptions.java").getAbsolutePath(),
		};
		
		try {
			TestDataExtractor.extractTestData(this.folder);
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		assertTrue(new File(this.folder.getRoot(), "src/com/pontetec/Main.java").isFile());
		
		JavacOptions options = new JavacOptions();
		try {
			options.parseOptions(args);
		} catch (IllegalArgumentException e) {
			assertNull("Exception should not be thrown.", e);
		} catch (ParseException e) {
			assertNull("Exception should not be thrown.", e);
		}
	}
	
	@Test
	public void testJavacOptions() {
		String[] args = new String[] {
				"-source",
				"1.5",
				"-target",
				"1.6",
				"-d",
				new File(folder.getRoot(), "build").getAbsolutePath(),
				"-encoding",
				"UTF-8",
				"-classpath",
				new File(folder.getRoot(), "lib/commons-cli-1.2.jar").getAbsolutePath(),
				"-sourcepath", 
				new File(folder.getRoot(), "src").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/Main.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/FileTest.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/TestOptions.java").getAbsolutePath(),
		};
		
		try {
			TestDataExtractor.extractTestData(this.folder);
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		JavacOptions options = new JavacOptions();
		try {
			options.parseOptions(args);
		} catch (IllegalArgumentException e) {
			assertNull("Exception should not be thrown.", e);
		} catch (ParseException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* verify argument parsing */
		assertThat(options.getEncoding(), equalTo("UTF-8"));
		assertThat(options.getSource(), equalTo("1.5"));
		assertThat(options.getTarget(), equalTo("1.6"));
		assertThat(options.getDestDir(), equalTo(new File(folder.getRoot(), "build").getAbsolutePath()));
		assertNotNull(options.getClasspath());
		assertThat(options.getClasspath().length, equalTo(1));
		assertThat(options.getClasspath()[0], equalTo(new File(folder.getRoot(), "lib/commons-cli-1.2.jar").getAbsolutePath()));
		assertNotNull(options.getSourcepath());
		assertThat(options.getSourcepath().length, equalTo(1));
		assertThat(options.getSourcepath()[0], equalTo(new File(folder.getRoot(), "src").getAbsolutePath()));
		assertNotNull(options.getSourceFiles());
		assertThat(options.getSourceFiles().length, equalTo(3));
		assertThat(Arrays.asList(options.getSourceFiles()), everyItem(containsString(new File(folder.getRoot(), "src/com/pontetec").getAbsolutePath())));
	}
	
	@Test
	public void testJavacOptionsWithProperties() {
		String[] args = new String[] {
				"-source",
				"1.5",
				"-target",
				"1.6",
				"-d",
				new File(folder.getRoot(), "build").getAbsolutePath(),
				"-encoding",
				"UTF-8",
				"-Dcom.pontetec.property.01=test01",
				"-Dcom.pontetec.property.02=test02",
				"-Dcom.pontetec.property.03=",
				"-Dcom.pontetec.property.04=TRUE",
				"-Dcom.pontetec.property.05=yes",
				"-classpath",
				new File(folder.getRoot(), "lib/commons-cli-1.2.jar").getAbsolutePath(),
				"-sourcepath", 
				new File(folder.getRoot(), "src").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/Main.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/FileTest.java").getAbsolutePath(),
				new File(folder.getRoot(), "src/com/pontetec/TestOptions.java").getAbsolutePath(),
		};
		
		try {
			TestDataExtractor.extractTestData(this.folder);
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		JavacOptions options = new JavacOptions();
		try {
			options.parseOptions(args);
		} catch (IllegalArgumentException e) {
			assertNull("Exception should not be thrown.", e);
		} catch (ParseException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* verify argument parsing */
		assertThat(options.getEncoding(), equalTo("UTF-8"));
		assertThat(options.getSource(), equalTo("1.5"));
		assertThat(options.getTarget(), equalTo("1.6"));
		assertThat(options.getDestDir(), equalTo(new File(folder.getRoot(), "build").getAbsolutePath()));
		assertNotNull(options.getClasspath());
		assertThat(options.getClasspath().length, equalTo(1));
		assertThat(options.getClasspath()[0], equalTo(new File(folder.getRoot(), "lib/commons-cli-1.2.jar").getAbsolutePath()));
		assertNotNull(options.getSourcepath());
		assertThat(options.getSourcepath().length, equalTo(1));
		assertThat(options.getSourcepath()[0], equalTo(new File(folder.getRoot(), "src").getAbsolutePath()));
		assertNotNull(options.getSourceFiles());
		assertThat(options.getSourceFiles().length, equalTo(3));
		assertThat(Arrays.asList(options.getSourceFiles()), everyItem(containsString(new File(folder.getRoot(), "src/com/pontetec").getAbsolutePath())));
		
		/* verify property type arguments */
		assertThat(System.getProperty("com.pontetec.property.01"), equalTo("test01"));
		assertThat(System.getProperty("com.pontetec.property.02"), equalTo("test02"));
		assertThat(System.getProperty("com.pontetec.property.03"), equalTo(""));
		assertThat(System.getProperty("com.pontetec.property.04"), equalTo("TRUE"));
		assertThat(System.getProperty("com.pontetec.property.05"), equalTo("yes"));
	}
}
