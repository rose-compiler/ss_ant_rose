/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.junit.rules.TemporaryFolder;

public class TestDataExtractor {
	
	private TestDataExtractor() {
		
	}
	
	public static void extractTestData(TemporaryFolder root) throws IOException {
		/* open the archive, and handle compression */
		InputStream rawStream = 
				TestDataExtractor.class.getResourceAsStream("project01.tar.gz");
		BufferedInputStream bufferedStream = new BufferedInputStream(rawStream);
		GzipCompressorInputStream compressionStream = new GzipCompressorInputStream(bufferedStream);
		TarArchiveInputStream archiveStream = new TarArchiveInputStream(compressionStream);
		
		/* iterate over entries in the archive */
		TarArchiveEntry tarEntry = null;
		while ((tarEntry = archiveStream.getNextTarEntry()) != null) {
			File entry = new File(root.getRoot(), tarEntry.getName());
			if (tarEntry.isDirectory()) {
				entry.mkdirs();
			} else if (tarEntry.isFile()) {
				/* file */
				entry.createNewFile();
				FileOutputStream fileStream = new FileOutputStream(entry);
				BufferedOutputStream bufferedFileStream = new BufferedOutputStream(fileStream);
				IOUtils.copy(archiveStream, bufferedFileStream, 8192);
				bufferedFileStream.flush();
				bufferedFileStream.close();
				fileStream.close();
			} else {
				throw new UnsupportedOperationException("Tarfile may only contain directories and files.  Symlinks, hardlinkes, etc. are not supported.");
			}
		}
		
		return;
	}
}
