/* ------------------------------Copyright-------------------------------------
 * NOTICE
 * This software (or technical data) was produced for the U. S.
 * Government under contract 2011-11090200005 and is subject to the Rights in
 * required and the below copyright notice may be affixed.
 * 
 * Copyright (c) 2013 Ponte Technologies. All Rights Reserved.
 * -----------------------------Copyright--------------------------------------
 */

package com.pontetec.test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.pontetec.ProcessException;

public class AntTest {
	@Rule
    public TemporaryFolder folder = new TemporaryFolder();
	
	@Test
	public void testSerialAntBuild() {
		String[] args = new String[] {
				"ant",
				"-lib",
				new File(System.getProperty("user.dir"), "dist/lib/").getAbsolutePath(),
				"-Dbuild.compiler=com.pontetec.RoseCompilerAdapter",
				"-Dcom.pontetec.rosecompiler.translator=/bin/echo",
				"compile"
		};
		
		try {
			TestDataExtractor.extractTestData(this.folder);
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* create the process output reader thread */
		ProcessStreamReader procOutput = null;
		Thread procOutputThread = null;
		
		/* create a process builder */
		ProcessBuilder procBuilder = new ProcessBuilder(args);
		procBuilder.directory(folder.getRoot());
		
		/* redirect stderr to sdtout to mae reading the output
		 * easier.
		 */
		procBuilder.redirectErrorStream(true);
		
		/* start the process */
		Process proc = null;
		try {
			proc = procBuilder.start();
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* create and start the stdout/stderr reader.  This will 
		 * only appear of the ant verbose switch is 
		 * provided: "-v" option. */
		procOutput = new ProcessStreamReader(proc.getInputStream());
		procOutputThread = new Thread(procOutput, Thread.currentThread().getName() + "-Reader");
		procOutputThread.start();
		
		/* wait for exit */
		try {
			proc.waitFor();
		} catch (InterruptedException ie) {
			/* set shutdown event on reader */
			procOutput.shutdown();
			/* this will rarely happen unless interrupted by the user. */
			assertNull("Exception should not be through by sub-process.", ie);
		}
		
		try {
			/* wait for the thread to exit */
			procOutputThread.join();
		} catch (InterruptedException ie) {
			/* swallow, we only wanted to attempt a shutdown */
		} finally {
			procOutputThread = null;
		}
		
		/* grab the return code */
		int procReturnCode = proc.exitValue();
		
		/* check the return code */
		assertThat(procReturnCode, is(0));
		
		/* check output */
		assertThat(procOutput.getOutput(), containsString("BUILD SUCCESSFUL"));
	}
	
	@Test
	public void testParallelAntBuild() {
		String[] args = new String[] {
				"ant",
				"-lib",
				new File(System.getProperty("user.dir"), "dist/lib/").getAbsolutePath(),
				"-Dbuild.compiler=com.pontetec.RoseCompilerAdapter",
				"-Dcom.pontetec.rosecompiler.translator=/bin/echo",
				"-Dcom.pontetec.rosecompiler.translator.jobs=4",
				"compile"
		};
		
		try {
			TestDataExtractor.extractTestData(this.folder);
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* create the process output reader thread */
		ProcessStreamReader procOutput = null;
		Thread procOutputThread = null;
		
		/* create a process builder */
		ProcessBuilder procBuilder = new ProcessBuilder(args);
		procBuilder.directory(folder.getRoot());
		
		/* redirect stderr to sdtout to mae reading the output
		 * easier.
		 */
		procBuilder.redirectErrorStream(true);
		
		/* start the process */
		Process proc = null;
		try {
			proc = procBuilder.start();
		} catch (IOException e) {
			assertNull("Exception should not be thrown.", e);
		}
		
		/* create and start the stdout/stderr reader.  This will 
		 * only appear of the ant verbose switch is 
		 * provided: "-v" option. */
		procOutput = new ProcessStreamReader(proc.getInputStream());
		procOutputThread = new Thread(procOutput, Thread.currentThread().getName() + "-Reader");
		procOutputThread.start();
		
		/* wait for exit */
		try {
			proc.waitFor();
		} catch (InterruptedException ie) {
			/* set shutdown event on reader */
			procOutput.shutdown();
			/* this will rarely happen unless interrupted by the user. */
			assertNull("Exception should not be through by sub-process.", ie);
		}
		
		try {
			/* wait for the thread to exit */
			procOutputThread.join();
		} catch (InterruptedException ie) {
			/* swallow, we only wanted to attempt a shutdown */
		} finally {
			procOutputThread = null;
		}
		
		/* grab the return code */
		int procReturnCode = proc.exitValue();
		
		/* check the return code */
		assertThat(procReturnCode, is(0));
		
		/* check output */
		assertThat(procOutput.getOutput(), containsString("BUILD SUCCESSFUL"));
	}
	
	private class ProcessStreamReader implements Runnable {
		/* GLOBALS AND DEFULATS */
		private static final String PROCESS_CHARSET = "ISO-8859-1";
		
		/**
		 * Input stream.
		 */
		private BufferedReader inputStream;
		
		/**
		 *  Shutdown flag.
		 */
		private volatile boolean shouldShutdown;
		
		/**
		 * Output String
		 */
		private StringBuilder output;
		
		private String lineSeparator;
		
		public ProcessStreamReader(InputStream is) {
			this.inputStream = new BufferedReader(new InputStreamReader(is, 
	        		Charset.forName(PROCESS_CHARSET)));
	        this.shouldShutdown = false;
	        this.output = new StringBuilder();
	        this.lineSeparator = System.getProperty("line.separator");
		}
		
		public String getOutput() {
			return this.output.toString();
		}
		
		/**
	     * Shutdown the thread safely.
	     */
	    public void shutdown() {
	    	this.shouldShutdown = true;
	    }
		
		/**
	     * Starts the thread.
	     */
	    public void run() {
	        try {
	            String line = null;
	            /* readline() will return null on EOF */
	            while ((line = this.inputStream.readLine()) != null 
	            		&& !this.shouldShutdown) {
	            	this.output.append(line);
	            	this.output.append(this.lineSeparator);
	            }
	        }
	        catch (IOException ioe) {
	            throw new ProcessException("Error reading process stdout/stderr.", ioe);
	        }
	    }
	}
}
